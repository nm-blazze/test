//extend place order with new calc

server.prepend('PlaceOrder', server.middleware.https, validateCartBundles.placeOrder, function (req, res, next) { 
    COHelpers.saveAllPromotionsId(); 
    next(); 
}); 


// promo logic
**
 * get all promotions for carrent order and save promotions ID, campaign manager
 */
function saveAllPromotionsId() {
    var collections = require('*/cartridge/scripts/util/collections');
    var currentBasket = BasketMgr.getCurrentBasket();
    var PriceAdjustment = require('dw/order/PriceAdjustment');
    var promotionsListId = [];
    var campaignManager = '';
    var campaignID = '';
    collections.forEach(currentBasket.getAllLineItems(), function (lineItem) {
        var li = lineItem;
        if (lineItem instanceof PriceAdjustment) {
            if ((empty(promotionsListId) || promotionsListId.indexOf(lineItem.promotionID) === -1)) {
                promotionsListId.push(lineItem.promotionID);
            }
            if((empty(campaignManager) && !empty(lineItem.campaign.custom.compaignChannelManager))) {
                    campaignManager = lineItem.campaign.custom.compaignChannelManager;
            }
            if((empty(campaignID) && !empty(lineItem.campaign.ID))) {
                campaignID = lineItem.campaign.ID;
            }
        }
    });

    //update corresponding lineitems
    collections.forEach(currentBasket.getAllProductLineItems(), function (lineItem) {
        if(lineItem.priceAdjustments.length) {
            collections.forEach(lineItem.priceAdjustments, function (pa) {
                var _p = pa;
                var _cond = promotionsListId.indexOf(pa.campaignID);
                var _pliid = promotionsListId;
                if (campaignID.indexOf(pa.campaignID) > -1) {
                    //saving campaign for easier filtering for stats
                    Transaction.wrap(function () {
                        lineItem.custom.campaignID = pa.campaignID;
                    });
                }
            });

        }
    });
    var promotionsId = !empty(promotionsListId) ? promotionsListId.join(', ') : '';

    Transaction.wrap(function () {
        currentBasket.custom.promotionID = promotionsId;
        currentBasket.custom.promotionAssignment = campaignManager;
        currentBasket.custom.campaignID = campaignID;
    });
}
