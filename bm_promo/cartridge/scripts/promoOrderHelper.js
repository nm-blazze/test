'use strict';

var Transaction = require('dw/system/Transaction');
var StringUtils = require('dw/util/StringUtils');
var Calendar = require('dw/util/Calendar');
var OrderMgr = require('dw/order/OrderMgr');

var logger = require('dw/system/Logger').getLogger('PromoLogger', 'PromoLogger');

/**
 * Method to fetch orders that meet search criteria
 * @param {string} orderNo - order number search criterion
 * @param {string} manager - return status search criterion
 * @param {string} returnMethod - return method search criterion
 * @returns {dw.util.SeekableIterator} orders - search result
 */
function queryPromotionOrders(manager, returnMethod) {
    var orders;

    var parts = [];
    var args = [];

    if (manager) {
        parts.push('custom.promotionAssignment={' + args.length + '}');
        args.push(manager);
    }

    if (returnMethod && returnMethod !== 'ANY') {
        parts.push('custom.campaignID={' + args.length + '}');
        args.push(returnMethod);
    }

    var queryStr = parts.join(' AND ');
    var searchArgs = [queryStr, 'creationDate desc'];
    searchArgs = searchArgs.concat(args);
    searchArgs.push(true);
    orders = OrderMgr.searchOrders.apply(OrderMgr.searchOrders, searchArgs);

    return orders;
}

/**
 * Decorates order record with various attributes to be used on storefront
 * @param {dw.order.Order} order - order system object
 * @param {Object} returnRecord - raw return record
 * @returns {Object}  return record model
 */
function getOrderModel(order, orderRecord) {
    /* eslint-disable no-param-reassign */
    var ProductMgr = require('dw/catalog/ProductMgr');
    var calendar = new Calendar(new Date(order.creationDate));

    calendar.setTimeZone(require('dw/system/Site').current.timezone);
    var date = StringUtils.formatCalendar(calendar, request.locale, Calendar.SHORT_DATE_PATTERN);
    var time = StringUtils.formatCalendar(calendar, request.locale, Calendar.TIME_PATTERN);
    var timezone = calendar.getTimeZone();
    orderRecord.creationDateFormatted = StringUtils.format('{0} {1} {2}', date, time, timezone);

    orderRecord.order = {
        UUID: order.UUID,
        orderNo: order.orderNo,
        dwOrder: order
    };

    return orderRecord;
    /* eslint-enable no-param-reassign */
}

var buildCampaigns = function () {
    var PromotionMgr = require('dw/campaign/PromotionMgr');
    var campaigns = PromotionMgr.getCampaigns().iterator();
    var managers = [];
    var managersDetails = [];
    var campaignsDetails = [];

    while (campaigns.hasNext()) {
        var campaign = campaigns.next();
        var manager = 'compaignChannelManager' in campaign.custom && !empty(campaign.custom.compaignChannelManager) && campaign.custom.compaignChannelManager !== null ? campaign.custom.compaignChannelManager : '';
        
        if(manager) {
            campaignsDetails.push({cid: campaign.ID, description: campaign.description, 'startDate': campaign.startDate, 'endDate': campaign.endDate, 'manager' : campaign.custom.compaignChannelManager, 'managerID': manager.getValue()})
            if (managers.indexOf(manager.getValue()) === -1) {
                managers.push(manager.getValue());
                managersDetails.push({mid: manager.getValue(), val: manager.getDisplayValue()});
            }            
        }        
    }

    return {
        managers : managersDetails,
        campaigns : campaignsDetails
    }
};


var getCampaignDetails = function (campaignID, orders) {
    var PromotionMgr = require('dw/campaign/PromotionMgr');
    var saleStats = buildCamaignRevenueDetails(orders, campaignID);
    var campainDetails = {
        campaign: PromotionMgr.getCampaign(campaignID),
        revenue: objToStr(saleStats.revenueTotal),
        customers: saleStats.customers,
        products: objToStr(saleStats.products)
    };
    return campainDetails;
}

function buildCamaignRevenueDetails (orders, campaignID) {
    var details = {}
    var revenueTotal = [];
    var revenueResult = '';
    var products = [];
    var customers = {
        registered : 0,
        unregistered: 0
    }
    while (orders.hasNext()) {
        var currntOrder = orders.next();
        var lis = currntOrder.getAllProductLineItems().iterator();
        while (lis.hasNext()) {
            var li = lis.next();
            //add only items related to specific campaign
            if('campaignID' in li.custom && li.custom.campaignID == campaignID) {
                var price = li.proratedPrice;
                var currencyCode = price.currencyCode;
                if (empty(revenueTotal[currencyCode])) {
                    revenueTotal[currencyCode] = 0;
                }
                revenueTotal[currencyCode] += parseInt(price.value.toFixed(2), 10);
                var productID = li.productID;
                var qty = li.quantityValue;
                if (empty(products[productID])) {
                    products[productID] = 0;
                }
                products[productID] += qty;
            }           
        }

        if (currntOrder.customer.profile !== null) {
            customers.registered++;
        } else {
            customers.unregistered++;
        }   
    }

    details.revenueTotal = revenueTotal;
    details.customers = customers;
    details.products = products;
    return details;
}

function objToStr(o){
    var str = '';
    for (var prop in o) {
        str += prop + ': ' + o[prop] + ';  ';
    }
    return str;
}

module.exports = {
    queryPromotionOrders: queryPromotionOrders,
    getOrderModel: getOrderModel,
    buildCampaigns : buildCampaigns,
    getCampaignDetails: getCampaignDetails
};
