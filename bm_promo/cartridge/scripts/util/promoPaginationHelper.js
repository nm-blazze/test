/**
 * Pagination Helper
 */

'use strict';

/**
 * Get pagination properties for order list
 * @param {Object} pagingModel -
 * @param {dw.web.HttpParameterMap} httpParameterMap -
 * @returns {Object} -
 */
function getPaginationProperty(pagingModel, httpParameterMap) {
    var pagination = pagingModel;
    var current = pagination.start;
    var totalCount = pagination.count;
    var pageSize = pagination.pageSize;
    var currentPage = pagination.currentPage;
    var maxPage = pagination.maxPage;
    var lr;
    var rangeBegin;
    var rangeEnd;

    var showingStart = current + 1;
    var showingEnd = current + pageSize;

    if (showingEnd > totalCount) {
        showingEnd = totalCount;
    }

    lr = 2;
    if (maxPage <= (2 * lr)) {
        rangeBegin = 1;
        rangeEnd = maxPage - 1;
    } else {
        rangeBegin = Math.max(Math.min(currentPage - lr, maxPage - (2 * lr)), 1);
        rangeEnd = Math.min(rangeBegin + (2 * lr), maxPage - 1);
    }

    var parameters = [];
    for (var property in httpParameterMap) { // eslint-disable-line
        if (property !== 'page') {
            parameters.push({
                key: property,
                value: httpParameterMap[property]
            });
        }
    }

    return {
        parameters: parameters,
        showingStart: showingStart,
        showingEnd: showingEnd,
        maxPage: maxPage,
        currentPage: currentPage,
        rangeBegin: rangeBegin,
        rangeEnd: rangeEnd,
        pageSize: pageSize,
        totalCount: totalCount,
        current: current
    };
}

// Module exports
module.exports = {
    getPaginationProperty: getPaginationProperty
};
