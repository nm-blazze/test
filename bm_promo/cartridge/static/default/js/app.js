jQuery(document).ready(function(){
    var allOpts = $('#campaigns option');

    jQuery('#channelManager').on('change', function(e){
        var manId = jQuery(this).val()
        if(manId != 'ANY') {
            jQuery('#campaigns option').prop('disabled',true);
            jQuery('#campaigns [data-manid*='+jQuery(this).find(':selected').data('manid')+']').prop('disabled', false);
        } else {
            jQuery('#campaigns option').prop('disabled',false);
            jQuery('#campaigns').val('ANY');
        }
    });

    function filterCampaigns (curCampaign) {
        console.log(curCampaign);
        return jQuery('#campaigns option').filter(function(){
            return jQuery.inArray(curCampaign, jQuery(this).data('manid')) > -1;
         });
    }

    jQuery('#channelManager').trigger('change');
});
