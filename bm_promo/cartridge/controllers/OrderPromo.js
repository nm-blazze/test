'use strict';

/*
 * BM extension cartridge controller for Promotions Order Menu
 *
 */

var URLUtils = require('dw/web/URLUtils');
var OrderMgr = require('dw/order/OrderMgr');
var StringUtils = require('dw/util/StringUtils');
var Resource = require('dw/web/Resource');
var logger = require('dw/system/Logger').getLogger('PromoLogger', 'PromoLogger');
var server = require('server');

/**
 * Method for rendering return order main menu for BM
 * Default page size is set as 10
 */
server.use(
    'List',
    function (req, res, next) {
        var PagingModel = require('dw/web/PagingModel');
        var promoOrderHelper = require('*/cartridge/scripts/promoOrderHelper');
        var paginationHelper = require('*/cartridge/scripts/util/promoPaginationHelper');
        var httpParameterMap = request.httpParameterMap;
        var currentCampaign = {};

        var pagingModel = null;
        var campaignID = httpParameterMap.campaigns.stringValue === null ? 'ANY' : httpParameterMap.campaigns.stringValue;
        var channelManager = httpParameterMap.channelManager.stringValue === null ? 'ANY' : httpParameterMap.channelManager.stringValue;
        
        var orders = promoOrderHelper.queryPromotionOrders(channelManager, campaignID);

        if (campaignID !== 'ANY') {
            currentCampaign = promoOrderHelper.getCampaignDetails(campaignID, promoOrderHelper.queryPromotionOrders(channelManager, campaignID));
        }

        var pageSize = httpParameterMap.pagesize.intValue !== null ? httpParameterMap.pagesize.intValue : 20;
        var currentPage = httpParameterMap.page.intValue ? httpParameterMap.page.intValue : 1;
        pageSize = pageSize === 0 ? 4000 : pageSize;
        pagingModel = new PagingModel(orders, orders.count);
        pagingModel.setPageSize(pageSize);
        pagingModel.setStart(pageSize * (currentPage - 1));

        var paginationProperty = paginationHelper.getPaginationProperty(pagingModel, httpParameterMap);
        var elements = pagingModel.pageElements;

        

        var orders = [];
        var StringUtils = require('dw/util/StringUtils');
        var promos = promoOrderHelper.buildCampaigns();

        while (elements.hasNext()) {
            var order = elements.next();
            orders.push(promoOrderHelper.getOrderModel(order, {}));
        }

        res.render('promo/promoOrdersHome', {
            currentCampaign: currentCampaign,
            orders: orders,
            PagingModel: pagingModel,
            paginationProperty: paginationProperty,
            channelManagers: promos.managers,
            channelManager: channelManager,
            campaignsWithManager: promos.campaigns,
            campaigns: campaignID
        });
        return next();
    }
);

server.get(
    'DetailsShow',
    function (req, res, next) {
        var promoOrderHelper = require('*/cartridge/scripts/promoOrderHelper');
        var orderNo = req.querystring.returnNo;
        var order = OrderMgr.getOrder(orderNo);
        if (order) {
            order = promoOrderHelper.getOrderModel(order, {})
        } else {
            var msg = StringUtils.format('Order #{0} incorrect', order.orderNo);
            logger.error(msg);
            res.render('promo/error', {
                errorMsg: msg,
                backUrl: URLUtils.https('OrderPromo-List')
            });
            return next();
        }

        res.render('promo/orderDetails', {
            order: order,
            currency: order.currencyCode
        });
        return next();
    }
);

server.post(
    'Delete',
    function (req, res, next) {
        var promoOrderHelper = require('*/cartridge/scripts/promoOrderHelper');
        var errors = [];
        var form = req.form;

        Object.keys(form)
            .filter(function (key) { return (key.indexOf('No') === 0); })
            .forEach(function (key) {
                var returnNo = form[key];
                var orderNo = No ? No.split('-')[0] : null;
                var order = OrderMgr.getOrder(orderNo);
                var result = ''
                if (!result.success && result.message) {
                    errors.push(result.message);
                }
            });

        var backUrl = URLUtils.https('OrderPromo-List');
        if (!errors.length) {
            res.redirect(backUrl);
        } else {
            res.render('promo/error', {
                errorMsg: errors.join('<br>'),
                backUrl: backUrl
            });
        }
        return next();
    }
);

module.exports = server.exports();
